package com.epf.demo.dao.repositories;

import com.epf.demo.dao.entities.Drive;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Optional;

public interface DriveRepository extends JpaRepository<Drive, Long> {
    Optional<Drive> findByAccount_Id(Long id_account);
    ArrayList<Drive> findListByAccount_Id(Long id_account);
}
