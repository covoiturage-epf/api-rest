package com.epf.demo.dao.repositories;

import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dao.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    Optional<Vehicle> findByAccount_Id(Long id_account);
}
