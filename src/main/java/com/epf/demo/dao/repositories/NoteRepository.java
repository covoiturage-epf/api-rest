package com.epf.demo.dao.repositories;

import com.epf.demo.dao.entities.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note, Long> {
}
