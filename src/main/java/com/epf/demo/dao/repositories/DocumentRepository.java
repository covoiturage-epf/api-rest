package com.epf.demo.dao.repositories;

import com.epf.demo.dao.entities.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document,Long> {
    Boolean existsDocumentByWording(String wording);
}
