package com.epf.demo.dao.repositories;

import com.epf.demo.dao.entities.Booking;
import com.epf.demo.dto.response.BookingDtoResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> findByAccount_Id(Long id_account);
    List<Booking> findByDrive_Id(Long id_drive);
    @Query("SELECT b FROM Booking b " +
            "JOIN FETCH b.drive d " +
            "JOIN FETCH d.account a " +
            "WHERE a.id = :id_driver")
    List<Booking> findBookingsByDriver(Long id_driver);
}