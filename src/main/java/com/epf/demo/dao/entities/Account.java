package com.epf.demo.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "account")
public class Account extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="password", length = 50)
    private String password;

    @Column(name="name", length = 50)
    private String name;

    @Column(name="first_name",length = 50)
    private String firstName;

    @Column(name="email",length = 50)
    private String email;

    @Column(name="phone",length = 50)
    private String phone;

    //role = 0 (false) for Passenger & 1 (true) for Driver
    private Boolean role;

    @JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "account")
    private List<Booking> bookings;

    @JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "account")
    private List<Note> notes;

    @JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "account")
    private List<Document> documents;

    @JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "account")
    private List<Vehicle> vehicles;

    @JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "account")
    private  List<Drive> drives;

    public Account (Account entity){
        this.setName(entity.getName());
        this.setFirstName(entity.getFirstName());
        this.setPassword(entity.getPassword());
        this.setEmail(entity.getEmail());
        this.setPhone(entity.getPhone());
        this.setRole(entity.getRole());
    }
}