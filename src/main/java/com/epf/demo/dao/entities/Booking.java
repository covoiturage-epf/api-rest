package com.epf.demo.dao.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "booking")
public class Booking extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "reserved_seats", length = 100)
    private Integer seats;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "id_passenger")
    private Account account;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "id_drive")
    private Drive drive;

    @Column(name = "status", length = 100)
    private Status status; // Nouvel attribut "status" de type enum

    @Column(name = "note", length = 100)
    // La note du booking est attribuée par le driver lorsque le drive a été effectué
    private Integer note;

    public enum Status {
        PENDING(0, "pending"),
        ACCEPTED(1, "accepted"),
        REFUSED(2, "refused"),
        CANCELED(3, "canceled"),
        CLOSED(4, "closed");

        private final int value;
        private final String viewName;

        Status(int value, String viewName) {
            this.value = value;
            this.viewName = viewName;
        }

        public Integer getValue() {
            return value;
        }

        public String getViewName() {
            return viewName;
        }
    }

    public void update(Booking entity) {
        this.setSeats(entity.getSeats());
        this.setAccount(entity.getAccount());
        this.setDrive(entity.getDrive());
        this.setStatus(entity.getStatus());
        this.setNote(entity.getNote());
    }

}
