package com.epf.demo.dao.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "drive")
public class Drive extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pickup", length = 50)
    private String pickup;

    @Column(name = "destination", length = 50)
    private String destination;

    @Column(name = "deptime", length = 50)
    private String deptime;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "seating", length = 100)
    private Integer seating;

    @Column(name = "details")
    private String details;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_driver")
    private Account account;

    @JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "drive")
    private List<Booking> bookings;

    @JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "drive")
    private List<Note> notes;

    public Drive(Drive entity){
        this.setId(entity.getId());
        this.setPickup(entity.getPickup());
        this.setDestination(entity.getDestination());
        this.setDeptime(entity.getDeptime());
        this.setPrice(entity.getPrice());
        this.setSeating(entity.getSeating());
        this.setDetails(entity.getDetails());
    }
}
