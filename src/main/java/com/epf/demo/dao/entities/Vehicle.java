package com.epf.demo.dao.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vehicle")
public class Vehicle extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="manufacturer", length = 50)
    private String manufacturer;

    @Column(name="model", length = 50)
    private String model;

    @Column(name="number_of_seats", length = 100)
    private Integer number_of_seats;

    @Column(name="color", length = 50)
    private String color;

    @Column(name="licence_plate", length = 50)
    private String licence_plate;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "id_account")
    private Account account;

    public void update(Vehicle entity){
        this.setManufacturer(entity.getManufacturer());
        this.setModel(entity.getModel());
        this.setNumber_of_seats(entity.getNumber_of_seats());
        this.setColor(entity.getColor());
        this.setLicence_plate(entity.getLicence_plate());
    }
}
