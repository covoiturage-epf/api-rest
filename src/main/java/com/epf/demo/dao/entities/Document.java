package com.epf.demo.dao.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "document")
public class Document extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="wording", length = 50)
    private String wording;

    @Column(name="url")
    private String url;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "id_account")
    private Account account;
}
