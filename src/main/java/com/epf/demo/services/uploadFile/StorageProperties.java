package com.epf.demo.services.uploadFile;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StorageProperties {

	/**
	 * Folder location for storing files
	 */
	private String location = "uploaded-files";

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
