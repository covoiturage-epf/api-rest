package com.epf.demo.services;

import com.epf.demo.dao.entities.Vehicle;
import com.epf.demo.exceptions.VehicleException;

import java.util.List;
import java.util.Optional;

public interface IVehicleService {
    Vehicle createVehicle(Vehicle vehicle);
    List<Vehicle> readVehicles();
    void updateVehicle(Vehicle vehicle) throws VehicleException;
    void deleteVehicle(Long id);
    Optional<Vehicle> findById(Long id);
    Optional<Vehicle> findByAccountId(Long Id_account);
}
