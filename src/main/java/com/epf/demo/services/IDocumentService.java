package com.epf.demo.services;

import com.epf.demo.dao.entities.Document;

import java.io.IOException;
import java.util.List;

public interface IDocumentService {
    void createDocument(Document document);

    List<Document> getAllDocuments();

    boolean updateDocument(Document document);

    void deleteDocument(Document document);

    Document findById(Long id);

    Boolean existByWording(String wording);

}
