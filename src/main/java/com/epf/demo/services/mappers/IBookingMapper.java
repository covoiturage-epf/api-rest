package com.epf.demo.services.mappers;

import com.epf.demo.dao.entities.Booking;
import com.epf.demo.dto.request.BookingDtoRequest;
import com.epf.demo.dto.response.BookingDtoResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "BookingMapperImpl")
public interface IBookingMapper {
    @Mapping(target = "accountId", source = "account.id")
    @Mapping(target = "driveId", source = "drive.id")
    BookingDtoResponse toDto(Booking booking);
    //Booking toEntity(BookingDtoRequest request);
}
