package com.epf.demo.services.mappers;

import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dto.request.DriveDtoRequest;
import com.epf.demo.dto.response.DriveDtoResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "DriveMapperImpl")
public interface IDriveMapper {
    @Mapping(target = "driverId", source = "account.id")
    DriveDtoResponse toDto(Drive drive);
    Drive toEntity(DriveDtoRequest request);
}
