package com.epf.demo.services.mappers;

import com.epf.demo.dao.entities.Note;
import com.epf.demo.dto.response.NoteDtoResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "NoteMapperImpl")
public interface INoteMapper {
    @Mapping(target = "accountId", source = "account.id")
    @Mapping(target = "driveId", source = "drive.id")
    NoteDtoResponse toDto(Note note);
}
