package com.epf.demo.services;

import com.epf.demo.dto.request.NoteDtoRequest;
import com.epf.demo.dto.response.NoteDtoResponse;

public interface INoteService {
    NoteDtoResponse createNote(NoteDtoRequest request);
    Double averageDriveRating(Long id_Drive); // Retourne la note moyenne d'une course
}
