package com.epf.demo.services;

import com.epf.demo.dao.entities.Notification;

public interface IEmailNotificationService {
    void sendNotification(Notification notification);
}
