package com.epf.demo.services;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.exceptions.AccountException;

import java.util.List;
import java.util.Optional;

public interface IAccountService {

  Account updateAccount(Account account) throws AccountException;

  List<Account> listAllAccount();
  Optional<Account> findById(Long id);
}
