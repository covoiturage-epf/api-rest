package com.epf.demo.services.impl;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dao.entities.Note;
import com.epf.demo.dao.repositories.AccountRepository;
import com.epf.demo.dao.repositories.DriveRepository;
import com.epf.demo.dao.repositories.NoteRepository;
import com.epf.demo.dto.request.NoteDtoRequest;
import com.epf.demo.dto.response.NoteDtoResponse;
import com.epf.demo.services.IDriveService;
import com.epf.demo.services.INoteService;
import com.epf.demo.services.mappers.INoteMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@Transactional
@RequiredArgsConstructor
public class NoteServiceImpl implements INoteService {

    private final NoteRepository repository;
    private final AccountRepository accountRepository;
    private final DriveRepository driveRepository;
    public final IDriveService driveService;
    private final INoteMapper mapper;

    @Override
    public NoteDtoResponse createNote(NoteDtoRequest request) {
        Optional<Account> passenger = accountRepository.findById(request.getAccountId());
        Optional<Drive> drive = driveRepository.findById(request.getDriveId());
        if (drive.isPresent() && passenger.isPresent()) {
            // Conversion de la chaîne en LocalDateTime
            LocalDateTime deptime = LocalDateTime.parse(drive.get().getDeptime(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            if (deptime.isBefore(LocalDateTime.now())) { // Vérification si le drive est déjà effectué
                Note note = Note.builder()
                        .account(passenger.get())
                        .drive(drive.get())
                        .value(request.getValue())
                        .build();
                return Stream.of(repository.save(note)).map(mapper::toDto).findFirst().get();
            } else {
                throw new IllegalArgumentException("Ce drive n'est pas encore effectué !");
            }
        } else {
            throw new IllegalArgumentException("Drive ou passager introuvable !");
        }
    }

    @Override
    public Double averageDriveRating(Long id_Drive) {
        Optional<Drive> optionalDrive = driveService.findById(id_Drive);
        if (optionalDrive.isPresent()) {
            Double averageRating = 0.0;
            Integer sum = 0;
            Drive drive = optionalDrive.get();
            List<Note> notes = drive.getNotes();
            if (notes != null && !notes.isEmpty()) {
                for (Note note : notes) {
                    if (note.getValue() != null) {
                        sum += note.getValue();
                    }
                }
                averageRating = (double) sum / notes.size();
            }
            return averageRating;
        } else {
            throw new IllegalArgumentException("Ce drive n'a pas été trouvé !");
        }
    }
}
