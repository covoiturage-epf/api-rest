package com.epf.demo.services.impl;

import com.epf.demo.dao.entities.Vehicle;
import com.epf.demo.dao.repositories.VehicleRepository;
import com.epf.demo.exceptions.VehicleException;
import com.epf.demo.services.IVehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class VehicleServiceImpl implements IVehicleService {

    private final VehicleRepository vehicleRepository;

    @Override
    public Vehicle createVehicle(Vehicle vehicle) {
        return vehicleRepository.save(vehicle);
    }

    @Override
    public List<Vehicle> readVehicles() {
        return vehicleRepository.findAll();
    }

    @Override
    public void updateVehicle(Vehicle vehicle) throws VehicleException {
        Optional<Vehicle> optionalVehicle = vehicleRepository.findById(vehicle.getId());
        if (optionalVehicle.isEmpty()) {
            throw new VehicleException("Véhicule introuvable");
        } else {
            optionalVehicle.get().update(vehicle);
            vehicleRepository.save(optionalVehicle.get());
        }
    }

    @Override
    public void deleteVehicle(Long id) {
        vehicleRepository.deleteById(id);
    }

    @Override
    public Optional<Vehicle> findById(Long id) {
        return vehicleRepository.findById(id);
    }

    @Override
    public Optional<Vehicle> findByAccountId(Long Id_account) {
        return vehicleRepository.findByAccount_Id(Id_account);
    }
}
