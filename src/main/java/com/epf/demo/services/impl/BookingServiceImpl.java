package com.epf.demo.services.impl;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.entities.Booking;
import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dao.entities.Notification;
import com.epf.demo.dao.repositories.AccountRepository;
import com.epf.demo.dao.repositories.BookingRepository;
import com.epf.demo.dao.repositories.DriveRepository;
import com.epf.demo.dto.request.BookingDtoRequest;
import com.epf.demo.dto.response.BookingDtoResponse;
import com.epf.demo.exceptions.DriveException;
import com.epf.demo.services.IBookingService;
import com.epf.demo.services.IDriveService;
import com.epf.demo.services.IEmailNotificationService;
import com.epf.demo.services.mappers.IBookingMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@Transactional
@RequiredArgsConstructor
public class BookingServiceImpl implements IBookingService {

    private final BookingRepository repository;
    private final AccountRepository accountRepository;
    private final DriveRepository driveRepository;
    private final IBookingMapper mapper;
    private final IDriveService driveService;
    private final IEmailNotificationService notificationService;


    @Override
    public BookingDtoResponse createBooking(BookingDtoRequest request) {
        Optional<Account> passenger = accountRepository.findById(request.getAccountId());
        Optional<Drive> drive = driveRepository.findById(request.getDriveId());
        if (drive.isPresent() && passenger.isPresent()) {
            if (drive.get().getSeating() >= request.getSeats() && request.getSeats() > 0) {
                Booking booking = Booking.builder()
                        .account(passenger.get())
                        .drive(drive.get())
                        .seats(request.getSeats())
                        .status(Booking.Status.PENDING)
                        .build();
                Notification notification = Notification.builder()
                        .recipient(drive.get().getAccount().getEmail())
                        .content(passenger.get().getFirstName()+" "+passenger.get().getName()+" vous adresse une demande de réservation pour la course à destination de "+drive.get().getDestination())
                        .build();
                notificationService.sendNotification(notification);
                return Stream.of(repository.save(booking)).map(mapper::toDto).findFirst().get();
            } else {
                throw new IllegalArgumentException("Nombre de place incorrect!");
            }
        } else {
            throw new IllegalArgumentException("drive or passenger unfounded!");
        }
    }

    @Override
    public void updateBooking(Long id_Booking, Integer status, Integer note) {
        Optional<Booking> optionalBooking = repository.findById(id_Booking);
        if (optionalBooking.isPresent()) {
            Booking existingBooking = optionalBooking.get();
            Optional<Account> passenger = accountRepository.findById(existingBooking.getAccount().getId());
            Optional<Drive> drive = driveRepository.findById(existingBooking.getDrive().getId());
            if (drive.isPresent() && passenger.isPresent()) {
                // Si le driver accepte la demande de réservation i.e statut == 1 qui signifie ACCEPTED
                if (status == 1) {
                    // On vérifie qu'il y'a encore assez de places
                    if (drive.get().getSeating() >= existingBooking.getSeats()) {
                        existingBooking.setSeats(existingBooking.getSeats());
                        existingBooking.setStatus(Booking.Status.ACCEPTED);
                        drive.get().setSeating(drive.get().getSeating() - existingBooking.getSeats());
                        driveService.updateDrive(drive.get());
                    } else {
                        //Annulation de la réservation
                        existingBooking.setSeats(0);
                        existingBooking.setStatus(Booking.Status.CANCELED);
                        throw new IllegalArgumentException("Ce nombre de places n'est plus disponible! Annulation de la réservation!");
                    }
                    repository.save(existingBooking);
                    Notification notification = Notification.builder()
                            .recipient(passenger.get().getEmail())
                            .content("Votre demande de réservation pour la course à destination de " + drive.get().getDestination() + " a été acceptée!")
                            .build();
                    notificationService.sendNotification(notification);
                }
                // Si le driver refuse la demande de réservertion
                else if (status == 2) {
                    existingBooking.setSeats(0);
                    existingBooking.setStatus(Booking.Status.REFUSED);
                    repository.save(existingBooking);
                    Notification notification = Notification.builder()
                            .recipient(passenger.get().getEmail())
                            .content("Votre demande de réservation pour la course à destination de " + drive.get().getDestination() + " a été rejetée!")
                            .build();
                    notificationService.sendNotification(notification);
                    // Si la réservation est annulée
                } else if (status == 3) {
                    existingBooking.setSeats(0);
                    existingBooking.setStatus(Booking.Status.CANCELED);
                    repository.save(existingBooking);
                    Notification notification = Notification.builder()
                            .recipient(passenger.get().getEmail())
                            .content("Votre demande de réservation pour la course à destination de " + drive.get().getDestination() + " a été annulée!")
                            .build();
                    notificationService.sendNotification(notification);
                } else if (status == 4) {
                    existingBooking.setSeats(0);
                    existingBooking.setStatus(Booking.Status.CLOSED);
                    repository.save(existingBooking);
                    Notification notification = Notification.builder()
                            .recipient(passenger.get().getEmail())
                            .content("Nous espérons que votre voyage à destination de " + drive.get().getDestination() + " s'est bien passé! Nous espérons vous revoir très bientôt!")
                            .build();
                    notificationService.sendNotification(notification);
                    updateBookingNote(existingBooking, note);
                } else {
                    throw new IllegalArgumentException("drive or passenger unfounded!");
                }
            } else {
                throw new DriveException("Réservation introuvable!");
            }
        }
    }
    @Override
    public void updateBookingNote(Booking booking, Integer note) {
        if (booking.getStatus() == Booking.Status.CLOSED) {
            booking.setNote(note);
        }else {
            throw new IllegalArgumentException("The booking must be canceled before calling this method!");
        }
    }

    @Override
    public void deleteBooking(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Booking> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<BookingDtoResponse> readAllBookingsByAccountId(Long Id_account) {
        List<Booking> bookings = repository.findByAccount_Id(Id_account);
        List<BookingDtoResponse> dtoResponses = new ArrayList<>();

        for (Booking booking : bookings) {
            BookingDtoResponse dtoResponse = mapper.toDto(booking);
            dtoResponses.add(dtoResponse);
        }

        return dtoResponses;
    }

    @Override
    public List<BookingDtoResponse> readAllBookingsByDriveId(Long Id_drive) {
        List<Booking> bookings = repository.findByDrive_Id(Id_drive);
        List<BookingDtoResponse> dtoResponses = new ArrayList<>();

        for (Booking booking : bookings) {
            BookingDtoResponse dtoResponse = mapper.toDto(booking);
            dtoResponses.add(dtoResponse);
        }

        return dtoResponses;
    }

    @Override
    public List<BookingDtoResponse> findBookingsByDriver(Long id_Driver) {

        List<Booking> bookings = repository.findBookingsByDriver(id_Driver);
        List<BookingDtoResponse> dtoResponses = new ArrayList<>();

        for (Booking booking : bookings) {
            BookingDtoResponse dtoResponse = mapper.toDto(booking);
            dtoResponses.add(dtoResponse);
        }
        return dtoResponses;
    }

}
