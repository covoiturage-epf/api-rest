package com.epf.demo.services.impl;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.repositories.AccountRepository;
import com.epf.demo.exceptions.AccountException;
import com.epf.demo.services.IAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class AccountServiceImpl implements IAccountService {

    private final AccountRepository accountRepository;


    // TODO: Compléter la méthode updateAccount...
    @Override
    public Account updateAccount(Account account) throws AccountException {
        return null;
    }

    @Override
    public List<Account> listAllAccount() {
        return accountRepository.findAll();
    }

    @Override
    public Optional<Account> findById(Long id) {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            return account;
        } else {
            return null;
        }
    }
}
