package com.epf.demo.services.impl;

import com.epf.demo.dao.entities.Notification;
import com.epf.demo.services.IEmailNotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class EmailNotificationService implements IEmailNotificationService {

    private final JavaMailSender javaMailSender;

    @Override
    public void sendNotification(Notification notification) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(notification.getRecipient());
        message.setSubject("Nouvelle notification");
        message.setText(notification.getContent());
        javaMailSender.send(message);
    }
}
