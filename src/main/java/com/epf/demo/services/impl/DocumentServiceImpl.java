package com.epf.demo.services.impl;

import com.epf.demo.dao.entities.Document;
import com.epf.demo.dao.repositories.DocumentRepository;
import com.epf.demo.services.IDocumentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class DocumentServiceImpl implements IDocumentService {

    private DocumentRepository documentRepository;

    @Override
    public void createDocument(Document document) {
        try {
            documentRepository.save(document);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Document> getAllDocuments() {
        try {
            return documentRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean updateDocument(Document document) {
        return false;
    }

    @Override
    public void deleteDocument(Document document) {
        try {
            documentRepository.delete(document);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Document findById(Long id) {
        try {
            documentRepository.findById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Boolean existByWording(String wording) {
        return documentRepository.existsDocumentByWording(wording);
    }

}
