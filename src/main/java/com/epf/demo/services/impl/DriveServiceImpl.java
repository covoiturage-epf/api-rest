package com.epf.demo.services.impl;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dao.repositories.AccountRepository;
import com.epf.demo.dao.repositories.DriveRepository;
import com.epf.demo.dto.request.DriveDtoRequest;
import com.epf.demo.dto.response.DriveDtoResponse;
import com.epf.demo.exceptions.DriveException;
import com.epf.demo.services.IDriveService;
import com.epf.demo.services.mappers.IDriveMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@Transactional
@RequiredArgsConstructor
public class DriveServiceImpl implements IDriveService {

    private final DriveRepository repository;
    private final AccountRepository accountRepository;

    private final IDriveMapper mapper;

    @Override
    public DriveDtoResponse createDrive(DriveDtoRequest request) {
        Optional<Account> driver = accountRepository.findById(request.getDriverId());
        if (driver.isPresent()) {
            //String dateString = request.getDeptime();
            //DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
            //LocalDateTime dateTime = LocalDateTime.parse(dateString, formatter);
            Drive drive = Drive.builder()
                    .account(driver.get())
                    .destination(request.getDestination())
                    .seating(request.getSeating())
                    .pickup(request.getPickup())
                    //.deptime(dateTime)
                    .deptime(request.getDeptime())
                    .details(request.getDetails())
                    .price(request.getPrice())
                    .build();
            DriveDtoResponse response = Stream.of(repository.save(drive)).map(mapper::toDto).findFirst().get();
            //System.out.println(response);
            return response;
        } else {
            throw new IllegalArgumentException("Driver unfounded!");
        }
    }

    @Override
    public List<DriveDtoResponse> readDrives() {
        List<Drive> drives = repository.findAll();
        List<DriveDtoResponse> dtoResponses = new ArrayList<>();
        for (Drive drive : drives) {
            DriveDtoResponse dtoResponse = Stream.of(repository.save(drive)).map(mapper::toDto).findFirst().get();
            System.out.println(dtoResponse);
            dtoResponses.add(dtoResponse);
        }
        return dtoResponses;
    }
    /*@Override
    public List<DriveDtoResponse> readDrives() {
        List<Drive> drives = repository.findAll();
        LocalDateTime currentDate = LocalDateTime.now();

        List<DriveDtoResponse> dtoResponses = drives.stream()
                .filter(drive -> drive.getDeptime().compareTo(currentDate) >= 0)
                .map(drive -> {
                    Drive savedDrive = repository.save(drive);
                    return mapper.toDto(savedDrive);
                })
                .collect(Collectors.toList());

        dtoResponses.forEach(System.out::println); // Afficher les réponses DTO

        return dtoResponses;
    }*/

    @Override
    public void updateDrive(Drive drive) throws DriveException {
        Optional<Drive> optionalDrive = repository.findById(drive.getId());
        if (optionalDrive.isPresent()) {
            Drive existingDrive = optionalDrive.get();
            existingDrive.setPickup(drive.getPickup());
            existingDrive.setDestination(drive.getDestination());
            existingDrive.setDeptime(drive.getDeptime());
            existingDrive.setPrice(drive.getPrice());
            existingDrive.setSeating(drive.getSeating());
            existingDrive.setDetails(drive.getDetails());
            repository.save(existingDrive);
        } else {
            throw new DriveException("Course introuvable");
        }
    }

    @Override
    public void deleteDrive(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Drive> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Optional<Drive> findByAccountId(Long Id_account) {
        return repository.findByAccount_Id(Id_account);
    }

    @Override
    public ArrayList<Drive> findListByAccount_Id(Long Id_account) {
        return repository.findListByAccount_Id(Id_account);
    }
}
