package com.epf.demo.services;

import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dto.request.DriveDtoRequest;
import com.epf.demo.dto.response.DriveDtoResponse;
import com.epf.demo.exceptions.DriveException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface IDriveService {
    DriveDtoResponse createDrive(DriveDtoRequest request);
    List<DriveDtoResponse> readDrives();
    void updateDrive(Drive drive) throws DriveException;
    void deleteDrive(Long id);
    Optional<Drive> findById(Long id);
    Optional<Drive> findByAccountId(Long Id_account);
    ArrayList<Drive> findListByAccount_Id(Long id_account);
}
