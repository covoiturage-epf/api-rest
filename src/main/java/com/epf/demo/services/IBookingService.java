package com.epf.demo.services;

import com.epf.demo.dao.entities.Booking;
import com.epf.demo.dto.request.BookingDtoRequest;
import com.epf.demo.dto.response.BookingDtoResponse;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface IBookingService {

    BookingDtoResponse createBooking(BookingDtoRequest booking);

    void updateBooking(Long id_Booking, Integer status, Integer note);

    void updateBookingNote(Booking booking, Integer note);

    void deleteBooking(Long id);

    Optional<Booking> findById(Long id);

    List<BookingDtoResponse> readAllBookingsByAccountId(Long Id_account);

    List<BookingDtoResponse> readAllBookingsByDriveId(Long Id_drive);

    List<BookingDtoResponse> findBookingsByDriver(Long id_Driver);
}
