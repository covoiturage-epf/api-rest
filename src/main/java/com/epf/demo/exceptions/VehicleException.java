package com.epf.demo.exceptions;

public class VehicleException extends RuntimeException{
    public VehicleException(String message) {
        super(message);
    }
}
