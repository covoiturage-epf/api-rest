package com.epf.demo.dto.response;

import com.epf.demo.dao.entities.Booking;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookingDtoResponse {
    private Long id;
    private Integer seats;
    private Long accountId;
    private Long driveId;
    private LocalDateTime reservationDate;
    private Booking.Status status;
}
