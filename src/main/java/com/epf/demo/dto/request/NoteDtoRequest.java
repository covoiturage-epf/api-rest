package com.epf.demo.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NoteDtoRequest {

    @NotNull(message = "Le paramètre value est obligatoire")
    private Integer value;

    @NotNull(message = "Le paramètre accountId est obligatoire")
    private Long accountId;

    @NotNull(message = "Le paramètre driveId est obligatoire")
    private Long driveId;
}
