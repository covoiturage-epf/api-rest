package com.epf.demo.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DriveDtoRequest {

    @NotEmpty(message = "Le paramètre pickup est obligatoire")
    private String pickup;

    @NotEmpty(message = "Le paramètre destination est obligatoire")
    private String destination;

    @NotEmpty(message = "Le paramètre departureTime est obligatoire")
    private String deptime;

    @NotNull(message = "Le paramètre price est obligatoire")
    private BigDecimal price;

    @NotNull(message = "Le paramètre availableSeats est obligatoire")
    private Integer seating;

    private String details;

    @NotNull(message = "Le paramètre account est obligatoire")
    private Long driverId;
}
