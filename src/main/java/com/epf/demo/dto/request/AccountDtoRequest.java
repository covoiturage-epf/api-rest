package com.epf.demo.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDtoRequest {
    @NotEmpty(message = "Le paramètre name est obligatoire")
    private String name;
    @NotEmpty(message = "Le paramètre firstName est obligatoire")
    private String firstName;
    @NotEmpty(message = "Le paramètre email est obligatoire")
    @Email(message = "Votre email est invalide")
    private String email;
    @NotEmpty(message = "Le paramètre password est obligatoire")
    private String password;
    @NotEmpty(message = "Le paramètre phone est obligatoire")
    private String phone;

    private Long passengerId;
    private Long driverId;
}
