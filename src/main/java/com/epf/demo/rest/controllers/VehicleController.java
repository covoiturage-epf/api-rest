package com.epf.demo.rest.controllers;

import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dao.entities.Vehicle;
import com.epf.demo.services.IVehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/vehicle/")
@CrossOrigin()
public class VehicleController {

    private final IVehicleService vehicleService;

    @PostMapping("saveVehicle")
    public Vehicle addVehicle(@RequestBody Vehicle vehicle){
        return vehicleService.createVehicle(vehicle);
    }
    @GetMapping("listVehicles")
    public List<Vehicle> listVehicles(){
        return vehicleService.readVehicles();
    }

    @PutMapping("editVehicle")
    public void editVehicle(@RequestBody Vehicle vehicle){
        vehicleService.updateVehicle(vehicle);
    }

    @DeleteMapping("deleteVehicle/{id}")
    public void deleteVehicle(@PathVariable Long id){
        vehicleService.deleteVehicle(id);
    }

    @GetMapping("findVehicleById/{id}")
    public Optional<Vehicle> findVehicleById(@PathVariable Long id){
        return vehicleService.findById(id);
    }

    @GetMapping("findVehicleByAccountId/{id}")
    public Optional<Vehicle> findVehicleByAccountId(@PathVariable Long id){
        return vehicleService.findByAccountId(id);
    }

}
