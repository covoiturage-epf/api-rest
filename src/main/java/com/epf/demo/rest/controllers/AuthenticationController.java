package com.epf.demo.rest.controllers;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.repositories.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@AllArgsConstructor
@CrossOrigin()
@RequestMapping("/api/authentication") //Lien vers l'API: http://localhost:8080/api/authentication
public class AuthenticationController {
    private final AccountRepository accountRepository;

    @PostMapping()
    public ResponseEntity<Optional<Account>> authenticate(@RequestBody Account account) {
        Optional<Account> account1 = accountRepository.findByEmailAndPassword(account.getEmail(), account.getPassword());
        if (account1.isEmpty()) {
            System.out.println("Aucun utilisateur trouvé");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } else {
            return ResponseEntity.ok(account1);
        }
    }
}
