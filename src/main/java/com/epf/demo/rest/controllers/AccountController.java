package com.epf.demo.rest.controllers;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.exceptions.AccountException;
import com.epf.demo.services.IAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/account/")
@CrossOrigin("*")
public class AccountController {

    @Autowired
    private final IAccountService accountService;

    @GetMapping("listAccounts")
    public ResponseEntity<List<Account>> readAll() throws AccountException {
        return ResponseEntity.ok(accountService.listAllAccount());
    }
    @GetMapping("findById/{id}")
    public Optional<Account> findById(@PathVariable Long id){
        return accountService.findById( id);
    }


}
