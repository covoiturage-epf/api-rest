package com.epf.demo.rest.controllers;

import com.epf.demo.dao.entities.Booking;
import com.epf.demo.dto.request.BookingDtoRequest;
import com.epf.demo.dto.response.BookingDtoResponse;
import com.epf.demo.services.IBookingService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/booking/")
@CrossOrigin()
@Validated
public class BookingController {

    private final IBookingService bookingService;

    @PostMapping("")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public BookingDtoResponse addBooking(@RequestBody @NotNull @Valid BookingDtoRequest request) {
        return bookingService.createBooking(request);
    }

    @GetMapping("myPassengerBookings/{id_Account}")
    public List<BookingDtoResponse> listBookingsByPassenger(@PathVariable("id_Account") Long id_Account) {
        return bookingService.readAllBookingsByAccountId(id_Account);
    }

    @GetMapping("DrivesBookings/{id_Drive}")
    public List<BookingDtoResponse> listBookingsByDrive(@PathVariable("id_Drive") Long id_Drive) {
        return bookingService.readAllBookingsByDriveId(id_Drive);
    }

    @PostMapping("update")
    public void updateBooking(@RequestParam Long id_Booking, @RequestParam Integer Status_value, @RequestParam Integer note_booking) {
         bookingService.updateBooking(id_Booking, Status_value,note_booking);
    }

    @GetMapping("myDriverBookings/{id_Driver}")
    public List<BookingDtoResponse> findBookingsByDriver(@PathVariable("id_Driver") Long id_Driver){
        return bookingService.findBookingsByDriver(id_Driver);
    }
}
