package com.epf.demo.rest.controllers;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.entities.Document;
import com.epf.demo.services.IAccountService;
import com.epf.demo.services.IDocumentService;
import com.epf.demo.services.uploadFile.FileSystemStorageService;
import com.epf.demo.services.uploadFile.StorageException;
import com.epf.demo.services.uploadFile.StorageProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.security.auth.login.AccountNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/document/")
@CrossOrigin()
public class DocumentController {

    private IDocumentService documentService;
    private IAccountService accountService;
    private Path rootLocation;

    private FileSystemStorageService fileSystemStorageService;

    public DocumentController(IDocumentService documentService, IAccountService accountService, StorageProperties storageProperties, FileSystemStorageService fileSystemStorageService) {
        this.documentService = documentService;
        this.accountService = accountService;
        this.rootLocation = Paths.get(storageProperties.getLocation());
        this.fileSystemStorageService = fileSystemStorageService;
    }

    @GetMapping("")
    public ResponseEntity<List<Document>> getAllDocuments() {
        List<Document> documents = documentService.getAllDocuments();
        if (documents != null) {
            return new ResponseEntity<>(documents, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Document> getDocumentById(@PathVariable("id") Long id) {
        Document document = documentService.findById(id);
        if (document != null) {
            return new ResponseEntity<>(document, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<Document> createDocument(@RequestParam("uploadedFile") MultipartFile file, @RequestParam(value = "id") Long id) throws IOException, AccountNotFoundException {
        Account driver = accountService.findById(id).orElseThrow(() -> new AccountNotFoundException("Account with id " + id + " not found"));
        if(!file.isEmpty()){
            Boolean verify = documentService.existByWording("Permis de conduire de "+driver.getName()+" "+driver.getFirstName());
            if(verify){
                throw new StorageException("Ce fichier a déjà été enregistré");
            }else {
                Path destinationFile = fileSystemStorageService.load(file.getOriginalFilename()).normalize().toAbsolutePath();
                if (!destinationFile.getParent().equals(this.rootLocation.toAbsolutePath())) {
                    // This is a security check
                    throw new StorageException("Cannot store file outside current directory.");
                }
                try (InputStream inputStream = file.getInputStream()) {
                    Files.copy(inputStream, destinationFile,
                            StandardCopyOption.REPLACE_EXISTING);
                }
                Document document = new Document();
                document.setWording("Permis de conduire de "+driver.getName()+" "+driver.getFirstName());
                document.setUrl(destinationFile.toString());
                document.setAccount(driver);
                documentService.createDocument(document);
                URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                        .buildAndExpand(document.getId()).toUri();
                return ResponseEntity.created(location).body(document);
            }
        }
        throw new StorageException("Le fichier est vide");
    }


    @PutMapping("/{id}")
    public ResponseEntity<Document> updateDocument(@PathVariable("id") Long id, @RequestBody Document document) {
        document.setId(id);
        boolean isUpdated = documentService.updateDocument(document);
        if (isUpdated) {
            return new ResponseEntity<>(document, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Document> deleteDocument(@PathVariable("id") Long id) {
        Document document = documentService.findById(id);
        if (document != null) {
            try {
                documentService.deleteDocument(document);
                return new ResponseEntity<>(document, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
