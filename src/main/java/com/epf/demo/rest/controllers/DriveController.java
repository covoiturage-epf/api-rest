package com.epf.demo.rest.controllers;

import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dto.request.DriveDtoRequest;
import com.epf.demo.dto.request.NoteDtoRequest;
import com.epf.demo.dto.response.DriveDtoResponse;
import com.epf.demo.dto.response.NoteDtoResponse;
import com.epf.demo.services.IDriveService;
import com.epf.demo.services.INoteService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/drive/")
@CrossOrigin()
public class DriveController {

    private final IDriveService driveService;
    private final INoteService noteService;

    @PostMapping("")
    public DriveDtoResponse addDrive(@RequestBody @NotNull @Valid DriveDtoRequest request) {
        return driveService.createDrive(request);
    }

    @PostMapping("addNote")
    public NoteDtoResponse AddDriveNote(@RequestBody @NotNull @Valid NoteDtoRequest request) {
        return noteService.createNote(request);
    }

    @GetMapping("")
    public List<DriveDtoResponse> listDrives() {
        return driveService.readDrives();
    }

    @PutMapping("editDrive")
    public void editDrive(@RequestBody Drive drive) {
        driveService.updateDrive(drive);
    }

    @DeleteMapping("deleteDrive/{id}")
    public void deleteDrive(@PathVariable Long id) {
        driveService.deleteDrive(id);
    }

    @GetMapping("findDriveById/{id}")
    public Optional<Drive> findDriveById(@PathVariable Long id) {
        return driveService.findById(id);
    }

    @GetMapping("findDriveByAccountId/{id}")
    public Optional<Drive> findDriveByAccountId(@PathVariable Long id) {
        return driveService.findByAccountId(id);
    }

    @GetMapping("findListDriveByAccountId/{id}")
    public ArrayList<Drive> findListDriveByAccountId(@PathVariable Long id) {
        return driveService.findListByAccount_Id(id);
    }

    @GetMapping("getAverageNote/{id_drive}")
    public Double getDriveAverageNote(@PathVariable Long id_drive) {
        return noteService.averageDriveRating(id_drive);
    }
}
