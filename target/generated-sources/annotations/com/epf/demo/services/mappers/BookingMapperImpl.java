package com.epf.demo.services.mappers;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.entities.Booking;
import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dto.response.BookingDtoResponse;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-14T16:04:32+0200",
    comments = "version: 1.5.1.Final, compiler: javac, environment: Java 17.0.4 (Amazon.com Inc.)"
)
@Component
public class BookingMapperImpl implements IBookingMapper {

    @Override
    public BookingDtoResponse toDto(Booking booking) {
        if ( booking == null ) {
            return null;
        }

        BookingDtoResponse bookingDtoResponse = new BookingDtoResponse();

        bookingDtoResponse.setAccountId( bookingAccountId( booking ) );
        bookingDtoResponse.setDriveId( bookingDriveId( booking ) );
        bookingDtoResponse.setId( booking.getId() );
        bookingDtoResponse.setSeats( booking.getSeats() );
        bookingDtoResponse.setStatus( booking.getStatus() );

        return bookingDtoResponse;
    }

    private Long bookingAccountId(Booking booking) {
        if ( booking == null ) {
            return null;
        }
        Account account = booking.getAccount();
        if ( account == null ) {
            return null;
        }
        Long id = account.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long bookingDriveId(Booking booking) {
        if ( booking == null ) {
            return null;
        }
        Drive drive = booking.getDrive();
        if ( drive == null ) {
            return null;
        }
        Long id = drive.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
