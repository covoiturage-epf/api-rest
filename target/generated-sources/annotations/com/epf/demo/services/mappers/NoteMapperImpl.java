package com.epf.demo.services.mappers;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dao.entities.Note;
import com.epf.demo.dto.response.NoteDtoResponse;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-14T16:04:32+0200",
    comments = "version: 1.5.1.Final, compiler: javac, environment: Java 17.0.4 (Amazon.com Inc.)"
)
@Component
public class NoteMapperImpl implements INoteMapper {

    @Override
    public NoteDtoResponse toDto(Note note) {
        if ( note == null ) {
            return null;
        }

        NoteDtoResponse noteDtoResponse = new NoteDtoResponse();

        noteDtoResponse.setAccountId( noteAccountId( note ) );
        noteDtoResponse.setDriveId( noteDriveId( note ) );
        noteDtoResponse.setId( note.getId() );
        noteDtoResponse.setValue( note.getValue() );

        return noteDtoResponse;
    }

    private Long noteAccountId(Note note) {
        if ( note == null ) {
            return null;
        }
        Account account = note.getAccount();
        if ( account == null ) {
            return null;
        }
        Long id = account.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long noteDriveId(Note note) {
        if ( note == null ) {
            return null;
        }
        Drive drive = note.getDrive();
        if ( drive == null ) {
            return null;
        }
        Long id = drive.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
