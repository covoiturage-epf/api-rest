package com.epf.demo.services.mappers;

import com.epf.demo.dao.entities.Account;
import com.epf.demo.dao.entities.Drive;
import com.epf.demo.dto.request.DriveDtoRequest;
import com.epf.demo.dto.response.DriveDtoResponse;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-14T16:04:31+0200",
    comments = "version: 1.5.1.Final, compiler: javac, environment: Java 17.0.4 (Amazon.com Inc.)"
)
@Component
public class DriveMapperImpl implements IDriveMapper {

    @Override
    public DriveDtoResponse toDto(Drive drive) {
        if ( drive == null ) {
            return null;
        }

        DriveDtoResponse driveDtoResponse = new DriveDtoResponse();

        driveDtoResponse.setDriverId( driveAccountId( drive ) );
        driveDtoResponse.setId( drive.getId() );
        driveDtoResponse.setPickup( drive.getPickup() );
        driveDtoResponse.setDestination( drive.getDestination() );
        driveDtoResponse.setDeptime( drive.getDeptime() );
        driveDtoResponse.setPrice( drive.getPrice() );
        driveDtoResponse.setSeating( drive.getSeating() );
        driveDtoResponse.setDetails( drive.getDetails() );

        return driveDtoResponse;
    }

    @Override
    public Drive toEntity(DriveDtoRequest request) {
        if ( request == null ) {
            return null;
        }

        Drive.DriveBuilder drive = Drive.builder();

        drive.pickup( request.getPickup() );
        drive.destination( request.getDestination() );
        drive.deptime( request.getDeptime() );
        drive.price( request.getPrice() );
        drive.seating( request.getSeating() );
        drive.details( request.getDetails() );

        return drive.build();
    }

    private Long driveAccountId(Drive drive) {
        if ( drive == null ) {
            return null;
        }
        Account account = drive.getAccount();
        if ( account == null ) {
            return null;
        }
        Long id = account.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
